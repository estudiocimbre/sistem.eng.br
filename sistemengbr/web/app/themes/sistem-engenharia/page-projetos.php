<?php
//Template Name: Projetos
get_header('pagina'); ?>
    <!-- INICIO DIVISÃO PAGINA -->
    <section>
        <div class="divisao-pagina2 bg2" style="background-image: url('<?php the_field('imagem_hero'); ?>')">
            <div class="container">
                <h1>Projetos</h1>
            </div>
        </div>
        <!-- /divisao-pagina -->
    </section>
    <!-- FIM DIVISÃO PAGINA -->
    <!-- INICIO PROJETOS -->
    <section>
        <div class="projetos">
            <div class="container">
                <div class="titulo_projetos">
                    <h1>Projetos</h1>
                    <p class="tagline">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean placerat, ex sit amet dignissim congue.</p>
                    <p
                        class="tagline">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque commodo tincidunt sem, id semper diam
                        pharetra ut. Sed viverra est ac ipsum sagittis tristique. Donec quam leo, cursus vitae laoreet euismod,
                        feugiat vitae dui. Sed tincidunt quam pharetra dui accumsan porttitor. Phasellus gravida dui dui,
                        id gravida metus finibus congue. Donec est magna, elementum at neque ut, tincidunt tempus lorem.
                        Sed ac aliquam metus, ac venenatis risus. Quisque eu erat at nunc euismod consequat vitae id amet.</p>
                </div>
                <!-- /titulo_projetos -->
            </div>
            <!-- /container -->
        </div>
        <!-- /projetos -->
    </section>
    <!-- FIM PROJETOS -->
    <!-- INICIO CLIENTES -->
    <section>
        <div class="clientes">
            <div class="conteudo_clientes">
                <div class="item_conteudo_clientes">
                    <figure class="logo-cliente">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/logo1.png" alt="Sistem Engenharia">
                        <figcaption>
                            <h3>Sistem Engenharia</h3>
                            <a href="#modal"></a>
                        </figcaption>
                    </figure>
                </div>
                <!-- /item_conteudo_clientes -->
                <div class="item_conteudo_clientes">
                    <figure class="logo-cliente">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/logo1.png" alt="Sistem Engenharia">
                        <figcaption>
                            <h3>Sistem Engenharia</h3>
                        </figcaption>
                    </figure>
                </div>
                <!-- /item_conteudo_clientes -->
                <div class="item_conteudo_clientes">
                    <figure class="logo-cliente">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/logo1.png" alt="Sistem Engenharia">
                        <figcaption>
                            <h3>Sistem Engenharia</h3>
                        </figcaption>
                    </figure>
                </div>
                <!-- /item_conteudo_clientes -->
                <div class="item_conteudo_clientes">
                    <figure class="logo-cliente">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/logo1.png" alt="Sistem Engenharia">
                        <figcaption>
                            <h3>Sistem Engenharia</h3>
                        </figcaption>
                    </figure>
                </div>
                <!-- /item_conteudo_clientes -->
                <div class="item_conteudo_clientes">
                    <figure class="logo-cliente">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/logo1.png" alt="Sistem Engenharia">
                        <figcaption>
                            <h3>Sistem Engenharia</h3>
                        </figcaption>
                    </figure>
                </div>
                <!-- /item_conteudo_clientes -->
                <div class="item_conteudo_clientes">
                    <figure class="logo-cliente">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/logo1.png" alt="Sistem Engenharia">
                        <figcaption>
                            <h3>Sistem Engenharia</h3>
                        </figcaption>
                    </figure>
                </div>
                <!-- /item_conteudo_clientes -->
                <div class="item_conteudo_clientes">
                    <figure class="logo-cliente">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/logo1.png" alt="Sistem Engenharia">
                        <figcaption>
                            <h3>Sistem Engenharia</h3>
                        </figcaption>
                    </figure>
                </div>
                <!-- /item_conteudo_clientes -->
                <div class="item_conteudo_clientes">
                    <figure class="logo-cliente">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/logo1.png" alt="Sistem Engenharia">
                        <figcaption>
                            <h3>Sistem Engenharia</h3>
                        </figcaption>
                    </figure>
                </div>
                <!-- /item_conteudo_clientes -->
            </div>
            <!-- /conteudo_clientes -->
        </div>
        <!-- /clientes -->
    </section>
    <!-- FIM CLIENTES -->
    <div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
        <!-- <button data-remodal-action="close" class="remodal-close" aria-label="Close"></button> -->
        <div class="projeto-modal">
            <div class="bg-modal" style="background: url('<?php echo get_stylesheet_directory_uri(); ?>/dist/img/bg-modal.jpg') no-repeat center">
                <div class="fechar-modal">
                    <button data-remodal-action="close" aria-label="Fechar">
                        <i class="fa fa-times" aria-hidden="true"></i>
                        <div class="esconde">
                            <div class="duro">Fechar</div>
                        </div>
                    </button>
                </div>
                <!-- /fechar-moda -->
            </div>
            <!-- /bg-modal -->
            <div class="infos-modal">
                <article>
                    <h1>Projeto Sistem</h1>
                    <p class="tagline">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec elit ante, porta non metus eget, imperdiet
                        rutrum eros. Quisque eu iaculis mi. Donec tempus luctus mi. Proin a iaculis tortor. Nunc diam augue,
                        dignissim malesuada pretium sed, congue eget neque. Sed volutpat mi quam. Donec cursus dui vitae
                        sem dictum, et tincidunt nisi laoreet. Nunc auctor, justo vel bibendum molestie, lacus elit venenatis
                        nulla, at tristique tortor leo sit amet nulla. Duis eleifend suscipit mauris eget pretium. Nullam
                        convallis luctus ligula, vel condimentum odio auctor quis. Ut in lacus vitae arcu viverra gravida.
                        Duis lacinia mollis massa ut blandit. Nullam eget sagittis eros, condimentum egestas nunc. Nulla
                        orci erat, tristique vel ipsum sit amet, aliquet vulputate odio.</p>
                </article>
            </div>
        </div>
        <!-- /projetos-modal -->
    </div>
    <!-- /remodal -->
    <?php get_footer('pagina'); ?>
