<?php 
$home_url = home_url('/');
define('INICIO', $home_url.'#inicio');
define('SOBRENOS', $home_url.'#sobre-nos');
define('SERVICOS', $home_url.'#servicos');
define('CLIENTES', $home_url.'#clientes');
define('CONTATO', $home_url.'#contato');
?>
<!DOCTYPE html>
<html lang="pt_BR">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Sistem Engenharia</title>
    <meta name="robots" content="index, follow">
    <!-- Favicon -->
    <link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/favicon-16.png" sizes="16x16" type="image/png">
    <link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/favicon-32.png" sizes="32x32" type="image/png">
    <link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/favicon-48.png" sizes="48x48" type="image/png">
    <link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/favicon-62.png" sizes="62x62" type="image/png">
    <link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/favicon-192.png" sizes="192x192" type="image/png">
    <!-- Depêndencias -->
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/dist/css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
    <!--[if lt IE 9]>
    	<script src="common/js/plugins/css3-mediaqueries.js"></script>
    <![endif]-->
    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
    <![endif]-->
    <?php wp_head(); ?> </head>

<body>
    <div id="fb-root"></div>
    <script>
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.9&appId=238372556544604";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

    </script>
    <!-- INICIO HEADER -->
    <section>
        <div class="heading">
            <div class="container">
                <div class="logo">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/logo.png" alt="[Sistem Engenharia]">
                </div>
                <!-- /logo -->
                <div class="menu-mobile">
                    <div class="hamburger-menu">
                        <div class="bar"></div>
                    </div>
                    <!-- /hamburger-menu -->
                    <div class="content-menu">
                        <ul>
                            <li class="item-hbg">
                                <a href="<?= INICIO; ?>">Início</a>
                            </li>
                            <li class="item-hbg">
                                <a href="<?= SOBRENOS; ?>">Sobre</a>
                            </li>
                            <li class="item-hbg">
                                <a href="<?= SERVICOS; ?>">Serviços</a>
                            </li>
                            <li class="item-hbg">
                                <a href="<?= CLIENTES; ?>">Clientes</a>
                            </li>
                            <li class="item-hbg">
                                <a href="<?= CONTATO; ?>">Contato</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /content-menu -->
                </div>
                <!-- /menu-mobile -->
                <div class="menu-desktop">
                    <ul class="item-menu">
                        <li>
                            <a href="<?= INICIO; ?>" data-hover="Início">Início</a>
                        </li>
                        <li>
                            <a href="<?= SOBRENOS; ?>" data-hover="Sobre">Sobre</a>
                        </li>
                        <li>
                            <a href="<?= SERVICOS; ?>" data-hover="Serviços">Serviços</a>
                        </li>
                        <li>
                            <a href="<?= CLIENTES; ?>" data-hover="Clientes">Clientes</a>
                        </li>
                        <li>
                            <a href="<?= CONTATO; ?>" data-hover="Contato">Contato</a>
                        </li>
                    </ul>
                    <!-- /item-menu -->
                </div>
                <!-- /menu-desktop -->
            </div>
            <!-- /container -->
        </div>
        <!-- /heading -->
    </section>
    <!-- FIM HEADER -->
</body>

</html>
