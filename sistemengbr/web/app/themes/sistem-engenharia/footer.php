<!-- INICIO CONTATO -->
<section>
    <div class="contato" id="contato">
        <div class="contato_esquerda">
            <div class="container">
                <h1>Contato</h1>
                <?php echo do_shortcode('[contact-form-7 id="16" title="Contato"]'); ?> 
            </div>
        </div>
        <!-- /contato_esquerda -->
        <div class="contato_direita">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3680.178103965371!2d-47.3394213851079!3d-22.721620585106276!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94c89bae6c29a113%3A0x5901f043a53b404b!2sSistem+Engenharia+e+Repres+de+Equip+Industriais!5e0!3m2!1spt-BR!2sbr!4v1527158593265" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>    
        </div>
        <!-- /contato_direita -->
    </div>
    <!-- /contato -->
</section>
<!-- FIM CONTATO -->

<!-- INICIO FOOTER -->
<footer>
    <div class="rodape">
        <div class="container">
            <div class="logo_rodape">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/logo-rodape.png" alt="[Sistem Engenharia]">
                <p class="tagline"> <?php the_field('endereco'); ?>
                </p>
            </div>
            <!-- /logo_rodape -->
            <div class="links_rodape">
                <div class="esquerda-links">
                    <ul>
                        <li>
                            <a href="<?= INICIO; ?>">Início</a>
                        </li>
                        <li>
                            <a href="<?= SOBRENOS; ?>">Sobre</a>
                        </li>
                        <li>
                            <a href="<?= SERVICOS; ?>">Serviços</a>
                        </li>
                    </ul>
                </div>
                <!-- /esquerda-links -->
                <div class="direita-links">
                    <ul>
                        <li>
                            <a href="<?= CLIENTES; ?>">Clientes</a>
                        </li>
                        <li>
                            <a href="<?= CONTATO; ?>">Contato</a>
                        </li>
                        <li>
                            <a href="https://www.facebook.com/sistemengenharia/">Facebook</a>
                        </li>
                    </ul>
                </div>
                <!-- /direita-links -->
            </div>
            <!-- /links_rodape -->
                    
            <div class="facebook_rodape">
                <div class="fb-page" data-href="https://www.facebook.com/sistemengenharia/" data-tabs="timeline" data-height="220" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                    <blockquote cite="https://www.facebook.com/sistemengenharia/" class="fb-xfbml-parse-ignore">
                        <a href="https://www.facebook.com/sistemengenharia/">Sistem Engenharia</a>
                    </blockquote>
                </div>
            </div>
            <!-- /facebook_rodape -->
        </div>
        <!-- /container -->
    </div>
    <!-- /rodape -->
    <div class="copyright">
        <div class="container">
            <div class="nome_copyright">
                <p>Sistem Engenharia © Todos os direitos reservados.</p>
            </div>
            <!-- /nome_copyright -->
            <div class="marca_copyright">
                <span class="copyright__text made-with-love">Desenvolvido com  <img class="icon-heart" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/icon-heart.svg" alt="amor"/>  por <a href="http://estudiocimbre.com.br" target="_blank" rel="noopener"><img class="cimbre-logo" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/cimbre-logo.svg" alt="cimbre"/></a></span>
            </div>
            <!-- /marca_copyright -->
        </div>
        <!-- /container -->
    </div>
    <!-- /copyright -->
</footer>
<!-- FIM FOOTER -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" integrity="sha256-pTxD+DSzIwmwhOqTFN+DB+nHjO4iAsbgfyFq5K5bcE0=" crossorigin="anonymous"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/dist/js/app.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/dist/js/remodal.min.js"></script>
<?php wp_footer(); ?>
