    <?php
        //Template Name: Início
        get_header();
    ?>
    <!-- INICIO SLIDE -->
    <section>
        <div class="slide" id="inicio">
            <?php echo do_shortcode('[rev_slider alias="slide"]'); ?> 
        </div>
    </section>
    <!-- FIM SLIDE -->
    <!-- INICIO SOBRE -->
    <section>
        <div class="sobre-nos" id="sobre-nos">
            <div class="foto_sobre-nos foto_sobre-nos_desktop">
                <img src="<?php the_field('foto_destaque'); ?>" alt="[Sobre Nós]">
            </div>
            <div class="texto_sobre-nos">
                <article>
                    <h1>Sobre</h1>
                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <p class="tagline">
                        <?php the_content(); ?> </p>
                    
                    <?php endwhile; else : ?>
                        <p><?php esc_html_e('Sem SOBRE'); ?></p>
                    <?php endif; ?>
                </article>
            </div>
            <div class="foto_sobre-nos foto_sobre-nos_mobile">
                <img src="<?php the_field('foto_destaque'); ?>" alt="[Sobre Nós]">
            </div>
        </div>
    </section>
    <!-- FIM SOBRE -->

    <!-- INICIO SERVIÇOS -->
    <section>
        <div class="servicos" id="servicos">
            <div class="container">
                <div class="titulo_servicos">
                    <h1>Serviços</h1>
                    <p class="tagline">
                        <?php the_field('servicos_descricao'); ?>
                    </p>
                </div>
                <!-- /titulo_servicos -->
                <div class="conteudo_servicos">
                    <?php
                        $args = array (
                        'post_type' => 'post',  // ou  post_type => 'post'
                        'order' => 'ASC',
                        'posts_per_page' => '6'
                        );
                    
                        $the_query = new WP_Query($args);
                    ?>
                    <?php if (have_posts()) : while ($the_query->have_posts()) : $the_query->the_post(); ?>
                    <div class="item_servicos">
                        <div class="icone_servicos">
                            <img src="<?php the_field('icone'); ?>" alt="[<?php the_title(); ?>]">
                        </div>
                        <!-- /icone_servicos -->
                        <div class="sobre_servicos">
                            <h2><?php the_title(); ?></h2>
                            <div class="servicos_desc-box">
                                <p class="tagline">
                                    <?php the_field('servico_descricao'); ?>
                                </p>
                            </div>
                        </div>
                        <div class="servicos__btn-box">
                            <a href="<?php the_permalink(); ?>" class="servicos__btn hvr-shutter-out-horizontal">Saiba Mais</a>
                        </div>
                        <!-- /sobre_servicos -->
                    </div>
                    <!-- /item_servicos -->
                    <?php endwhile; ?>
                    <?php endif; ?>
                    <?php wp_reset_query(); ?> 
                </div>
                <!-- /conteudo_servicos -->
            </div>
        <!-- /container -->
        </div>
    <!-- /servicos -->
    </section>
    <!-- FIM SERVIÇOS -->

    <!-- INICIO CLIENTES -->
    <section>
        <div class="clientes" id="clientes">
            <div class="container">
                <div class="titulo_clientes">
                    <h1>Clientes</h1>
                    <p class="tagline"><?php the_field('clientes-descricao'); ?></p>
                </div>
                <!-- /titulo_clientes -->
            </div>
            <!-- /container -->
            <!-- conteudo_clientes -->
            <div class="conteudo_clientes owl-carousel owl-theme">
                <?php if (have_rows('clientes-inicio')) : while (have_rows('clientes-inicio')) : the_row(); ?>
                <div class="item item_conteudo_clientes">
                    <figure class="logo-cliente">
                        <img src="<?php the_sub_field('logo'); ?>" alt="<?php the_sub_field('nome-cliente'); ?>">
                        <figcaption>
                            <h3><?php the_sub_field('nome-cliente'); ?></h3>
                        </figcaption>
                    </figure>
                </div>
                <!-- /item_conteudo_clientes -->
                <?php endwhile; else : endif; ?> 
            </div>
            <!-- /conteudo_clientes -->
        </div>
        <!-- /clientes -->
    </section>
    <!-- FIM CLIENTES -->
    
    <!-- INICIO DIVISÃO PAGINA -->
    <section>
        <div class="divisao-pagina bg1" style="background-image: url('<?php the_field('imagem_paralax'); ?>')">
            <div class="container">
                <div class="frase_home-box">
                    <h1 class="frase_home"><?php the_field('frase_home'); ?></h1>
                </div>
            </div>
        </div>
        <!-- /divisao-pagina -->
    </section>
    <!-- FIM DIVISÃO PAGINA -->
    <?php get_footer(); ?>