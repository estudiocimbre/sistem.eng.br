<?php
get_header('pagina'); ?>
    <!-- INICIO DIVISÃO PAGINA -->
    <section>
        <div class="divisao-pagina2 bg2 mobile" style="background-image: url('<?php the_field('imagem_hero_mobile'); ?>')">
            <div class="container">
                <!--
                <h1>
                    <?php the_title(); ?>
                </h1>
                
                <p class="tagline">
                    <?php the_excerpt(); ?> 
                </p>
                -->
            </div>
        </div>
        <div class="divisao-pagina2 bg2 desktop" style="background-image: url('<?php the_field('imagem_hero'); ?>')">
            <div class="container">
                <!--
                <h1>
                    <?php the_title(); ?>
                </h1>
                
                <p class="tagline">
                    <?php the_excerpt(); ?> 
                </p>
                -->
            </div>
        </div>
        <!-- /divisao-pagina -->
    </section>
    <!-- FIM DIVISÃO PAGINA -->
    <!-- INICIO PROJETOS -->
    <section>
        <div class="projetos">
            <div class="container">
                <div class="titulo_projetos">
                    <h1>
                        <?php the_title(); ?>
                    </h1>
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                    <p class="tagline">
                        <?php the_content(); ?> 
                    </p>
                    <?php endwhile; else: ?>
                    <?php endif; ?> 
                </div>
                <!-- /titulo_projetos -->
            </div>
            <!-- /container -->
        </div>
        <!-- /projetos -->
    </section>
    <!-- FIM PROJETOS -->
    <!-- INICIO CLIENTES -->
    <section class="projetos-clientes">
        <div class="clientes">
            <div class="conteudo_clientes">
                <?php if(have_rows('modal')): while(have_rows('modal')) : the_row(); ?>
                <div class="item_conteudo_clientes">
                    <figure class="logo-cliente">
                        <img src="<?php the_sub_field('logo-modal'); ?>" alt="<?php the_sub_field('nome-modal'); ?>">
                        <figcaption>
                            <h3>
                                <?php the_sub_field('nome-modal'); ?>
                            </h3>
                            <a href="#<?php the_sub_field('slug'); ?>"></a>
                        </figcaption>
                    </figure>
                </div>
                <!-- /item_conteudo_clientes -->
                <div class="remodal" data-remodal-id="<?php the_sub_field('slug'); ?>" role="dialog" aria-labelledby="modal1Title"
                    aria-describedby="modal1Desc">
                    <div class="projeto-modal">
                        <div class="bg-modal" style="background: url('<?php the_sub_field('imagem_destaque'); ?>') no-repeat center">
                            <div class="fechar-modal">
                                <button data-remodal-action="close" aria-label="Fechar">
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                    <div class="esconde">
                                        <div class="duro">Fechar</div>
                                    </div>
                                </button>
                            </div>
                            <!-- /fechar-moda -->
                        </div>
                        <!-- /bg-modal -->
                        <div class="infos-modal">
                            <article>
                                <h1>
                                    <?php the_sub_field('nome-modal') ?>
                                </h1>
                                <p class="tagline">
                                    <?php the_sub_field('texto-modal'); ?> </p>
                            </article>
                        </div>
                    </div>
                    <!-- /projetos-modal -->
                </div>
                <!-- /remodal -->
                <?php endwhile; else : endif; ?> </div>
            <!-- /conteudo_clientes -->
        </div>
        <!-- /clientes -->
    </section>
    <!-- FIM CLIENTES -->
    <?php get_footer('pagina'); ?>
