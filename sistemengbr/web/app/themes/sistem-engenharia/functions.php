<?php 
// Funções para Limpar o Header
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'start_post_rel_link', 10, 0 );
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('admin_print_scripts', 'print_emoji_detection_script');
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('admin_print_styles', 'print_emoji_styles');

// LIMITAR RESUMO 
function novo_tamanho_do_resumo($length) {
	return 12;
}
add_filter('excerpt_length', 'novo_tamanho_do_resumo');



function new_excerpt_more($more) {
	return '...';
}
 
add_filter('excerpt_more', 'new_excerpt_more');

function change_post_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'Serviços';
    $submenu['edit.php'][5][0] = 'Serviços';
    $submenu['edit.php'][10][0] = 'Adicionar Serviço';
    $submenu['edit.php'][16][0] = 'Tags';
    echo '';
}
function change_post_object() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'Serviços';
    $labels->singular_name = 'Serviço';
    $labels->add_new = 'Adicionar Serviço';
    $labels->add_new_item = 'Adicionar Serviço';
    $labels->edit_item = 'Editar Serviço';
    $labels->new_item = 'Serviço';
    $labels->view_item = 'Ver Serviço';
    $labels->search_items = 'Buscar Serviços';
    $labels->not_found = 'Nenhum Serviço encontrado';
    $labels->not_found_in_trash = 'Nenhum Serviço encontrado no Lixo';
    $labels->all_items = 'Todos Serviços';
    $labels->menu_name = 'Serviços';
    $labels->name_admin_bar = 'Serviços';
}
 
add_action( 'admin_menu', 'change_post_label' );
add_action( 'init', 'change_post_object' );

/**
 * Disable WP auto compressing JPEG files
 */
add_filter('jpeg_quality', create_function('', 'return 100;'));

?>